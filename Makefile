APP=helloflask-romk
PYZ=helloflask.pyz

.PHONY: build deploy undeploy test clean


build: $(PYZ)

deploy: build
	cf push -m 128M -b python_buildpack -c 'python -m gunicorn.app.wsgiapp helloflask.helloflask:app' -p $(PYZ) $(APP)

undeploy:
	cf delete -f $(APP)

test:
	curl -sS $(APP).cfapps.io
	@echo

clean:
	rm -rf $(PYZ)
	rm -rf zip

$(PYZ): zip
	python -m zipapp $< -o $@ -m 'gunicorn.app.wsgiapp:run'

zip:
	pip install -t $@ .
	pip install --upgrade -t $@ gunicorn
	find $@ -type d -name __pycache__ | xargs rm -rf
	find $@ -type d -name '*.dist-info' | xargs rm -rf
	find $@ -type f -name '*.pyc' -delete
