# helloflask

Create a self-contained binary file with the flask app.
Packs the web app and all its dependencies into a `.pyz` file.


## Build a fat-binary

```
make build
```

## Deploy the binary to Cloud Foundry

```
make deploy
```
